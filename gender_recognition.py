import tensorflow as tf
from sklearn.neural_network import MLPClassifier
from utils import read_data
from voice import Voice

data_path = '/Users/ricardoocampo/Workspace/crabi_test/data/voice.csv'
data = Voice(data_path)


def tensorflow_approach():
    n_nodes = 10
    stddev = 0.5
    x = tf.placeholder(tf.float32, [None, 20])
    W1 = tf.Variable(tf.random_normal([20, n_nodes], stddev=stddev))
    W2 = tf.Variable(tf.random_normal([n_nodes, 1], stddev=stddev))
    b1 = tf.Variable(tf.random_normal([n_nodes], stddev=stddev))
    b2 = tf.Variable(tf.random_normal([1], stddev=stddev))
    layer1 = tf.nn.sigmoid(tf.matmul(x, W1) + b1)
    y = tf.nn.sigmoid(tf.matmul(layer1, W2) + b2)

    y_ = tf.placeholder(tf.float32, [None, 1])

    cross_entropy = tf.reduce_mean(
          tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))
    train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)

    sess = tf.InteractiveSession()
    tf.global_variables_initializer().run()
    # Train
    for _ in range(1000):
        batch_xs, batch_ys = data.next_batch(100)
        sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})

    # Test trained model
    correct_prediction = tf.equal(tf.round(y), y_)

    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    model_accuracy = sess.run(accuracy, feed_dict={x: data.train[0],
                                                   y_: data.train[1]})
    return model_accuracy


def sklearn_approach():
    clf = MLPClassifier(
        solver='lbfgs', alpha=1e-5,
        hidden_layer_sizes=(5, 2), random_state=1
    ).fit(data.train[0], data.train[1])
    return clf.score(data.test[0], data.test[1])


if __name__ == '__main__':
    print("The result with tensorflow is:", tensorflow_approach())
    print("The result with sklearn is:", sklearn_approach())
