import pandas as pd
from sklearn import preprocessing


tag_name = 'label'


def read_data(file_path):
    df = pd.read_csv(filepath_or_buffer=file_path)
    return _process_data(df)


def _process_data(df):
    X, y = _divide_variables_label(df, tag_name)
    return X.as_matrix(), _binarize_array(y.as_matrix())


def _binarize_array(nominal_variable):
    lb = preprocessing.LabelBinarizer()
    return lb.fit_transform(nominal_variable)


def _divide_variables_label(df, column_name):
    return df.drop([column_name], axis=1), df[column_name]


if __name__ == '__main__':
    file_path = '/Users/ricardoocampo/Workspace/crabi_test/data/voice.csv'
    df = read_data(file_path)
    print(_process_data(df))
