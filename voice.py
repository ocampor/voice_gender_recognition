from utils import read_data
from sklearn.preprocessing import normalize
import random


class Voice:
    def __init__(self, file_path):
        self._file_path = file_path
        self._X, self._y = read_data(file_path)
        self._preprocessing()
        datasets = self._split_dataset(0.7)
        self.train = datasets['train']
        self.test = datasets['test']

    def next_batch(self, batch_size):
        dataset_size = len(self.train[0])
        random_sample = random.sample(range(dataset_size), batch_size)
        return self.train[0][random_sample], self.train[1][random_sample]

    def _preprocessing(self):
        self._X = normalize(self._X, norm='l1')

    def _split_dataset(self, p=0.7):
        dataset_size = len(self._y)
        index = list(range(dataset_size))
        random.shuffle(index)
        train_index = index[:round(dataset_size * p)]
        test_index = index[round(dataset_size * p):]
        train_set = self._X[train_index], self._y[train_index]
        test_set = self._X[test_index], self._y[test_index]
        return {
            'train': train_set,
            'test': test_set
        }


if __name__ == '__main__':
    file_path = '/Users/ricardoocampo/Workspace/crabi_test/data/voice.csv'
    v = Voice(file_path)
    print(v.next_batch(100))
